# Set macOS Preferences
if platform?('mac_os_x')
  macos_userdefaults 'Move the macOS dock to the left' do
    domain 'com.apple.dock'
    key 'orientation'
    type 'string'
    value 'left'
  end

  macos_userdefaults 'Autohide the macOS dock' do
    domain 'com.apple.dock'
    key 'autohide'
    type 'bool'
    value 'true'
  end

  macos_userdefaults 'Enable macOS firewall' do
    domain '/Library/Preferences/com.apple.alf'
    key 'globalstate'
    value 1
  end
end
