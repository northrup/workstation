# frozen_string_literal: true

# Fetch my dotfiles
git '/Users/jjn/.valhalla' do
  repository 'git@gitlab.com:northrup/dotfiles.git'
  revision 'master'
  enable_submodules true
  action :checkout
  user 'jjn'
  group 'staff'
end

# Create Symlinks for files and directories
# .config directory
link '/Users/jjn/.config' do
  to '/Users/jjn/.valhalla/config'
end

# .local directory
link '/Users/jjn/.local' do
  to '/Users/jjn/.valhalla/local'
end

# git configuration files
link '/Users/jjn/.gitconfig' do
  to '/Users/jjn/.valhalla/git/gitconfig'
end
link '/Users/jjn/.gitignore' do
  to '/Users/jjn/.valhalla/git/gitignore'
end

# tmux configuration files
link '/Users/jjn/.tmux.conf' do
  to '/Users/jjn/.valhalla/tmux/tmux.conf'
end
link '/Users/jjn/.tmux.conf.user' do
  to '/Users/jjn/.valhalla/tmux/tmux.conf.user'
end

# zsh configuration files
link '/Users/jjn/.zhelrc' do
  to '/Users/jjn/.valhalla/zsh/hel/core/zhelrc'
end
link '/Users/jjn/.zlogin' do
  to '/Users/jjn/.valhalla/zsh/hel/core/zlogin'
end
link '/Users/jjn/.zlogout' do
  to '/Users/jjn/.valhalla/zsh/hel/core/zlogout'
end
link '/Users/jjn/.zprofile' do
  to '/Users/jjn/.valhalla/zsh/hel/core/zprofile'
end
link '/Users/jjn/.zshenv' do
  to '/Users/jjn/.valhalla/zsh/hel/core/zshenv'
end
link '/Users/jjn/.zshrc' do
  to '/Users/jjn/.valhalla/zsh/hel/core/zshrc'
end
