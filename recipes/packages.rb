# Install MacOS Things...
if platform?('mac_os_x')
  # Environment Base
  homebrew_tap 'homebrew/bundle'
  homebrew_tap 'homebrew/cask'
  homebrew_tap 'homebrew/core'
  homebrew_tap 'homebrew/services'
  homebrew_tap 'homebrew/cask-fonts'
  homebrew_package %w[git zsh neovim tmux gnupg pinentry-mac]
  # Utilities and Tools
  homebrew_package %w[httpie coreutils git-quick-stats gnu-sed jq libssh2 
    oath-toolkit socat ssh-copy-id the_silver_searcher htop ssh-copy-id trash
    pwgen watch vault ctags diff-so-fancy dive git-lfs nmap rclone mas hstr]
  homebrew_cask 'iterm2'
  homebrew_cask 'alacritty'
  homebrew_cask 'chef-workstation'
  homebrew_cask 'font-fira-code'
  homebrew_cask 'font-hack-nerd-font'
  # Networking Tools
  homebrew_package %w[mtr iperf3 nebula]
  # Developer Tools
  homebrew_package %w[go asdf]
  # Fun Packages
  homebrew_package %w[cmatrix fortune newsboat]
  homebrew_cask 'aerial'
  # Kubernetes Tools
  homebrew_tap 'derailed/k9s'
  homebrew_tap 'datawire/blackbird'
  homebrew_package %w[helm kubernetes-cli k9s kind cfssl]
end

if platform?('debian', 'ubuntu')
  # Environment Base
  # Utilities and Tools
  # Networking Tools
  # Developer Tools
  # Fun Packages
  # Kubernetes Tools
end
