#
# Cookbook:: workstation
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.
#
if platform?('mac_os_x')
  include_recipe 'chef-client::launchd_service'
else
  include_recipe 'chef-client::service'
end

if platform?('mac_os_x')
  include_recipe 'homebrew::default'
end

include_recipe 'workstation::packages'
include_recipe 'workstation::preferences'
include_recipe 'workstation::dotfiles'
