name 'workstation'
maintainer 'John Northrup'
maintainer_email 'john@8bitwizard.net'
license 'All Rights Reserved'
description 'Installs/Configures workstation'
version '0.1.15'
chef_version '>= 17.0'

depends 'homebrew', '~> 5.3.0'
depends 'chef-client', '~> 12.3.4'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/workstation/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/workstation'
